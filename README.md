# Shopping list
This app is a simple shopping list. The user can add items with a name and optionally a price. The list can be sorted and is kept in the shared preferences.

The app also uses the `declarativeui` library to create its views. 

You read the comments in the code for more details.


## Screenshot
![Screenshot](https://gitlab.com/femastudios/android/dataflow-examples/dataflowdemo-shoppinglist/raw/master/screenshot.png)

## Install
You can go to [releases](https://gitlab.com/femastudios/android/dataflow-examples/dataflowdemo-shoppinglist/-/releases) to find an APK to install.