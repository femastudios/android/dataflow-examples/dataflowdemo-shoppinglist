package com.femastudios.android.dataflowdemo.shoppinglist

import android.os.Bundle
import android.preference.PreferenceManager
import android.text.InputType
import android.view.Gravity
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.setMargins
import androidx.core.view.setPadding
import com.femastudios.android.core.context
import com.femastudios.android.core.dp
import com.femastudios.dataflow.android.declarativeui.*
import com.femastudios.dataflow.android.preferences.PreferenceField
import com.femastudios.dataflow.android.preferences.getStringField
import com.femastudios.dataflow.android.viewState.extensions.setVisible
import com.femastudios.dataflow.extensions.*
import com.google.android.material.floatingactionbutton.FloatingActionButton
import org.json.JSONArray
import java.math.BigDecimal
import java.math.RoundingMode

class MainActivity : AppCompatActivity() {

    /**
     * This is the field representing the shopping list
     * Notice that it's an instance of [PreferenceField] because the list must be saved in persistent memory
     */
    lateinit var list: PreferenceField<List<Item>>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Here we assign the list PreferenceField
        //Since android preferences can only represent primitive values
        //we will actually save a string that is a JSON representation of our shopping list
        list = PreferenceManager.getDefaultSharedPreferences(this)
            .getStringField(
                key = "list", //The key that represents this list in the preferences
                defaultValue = emptyList<Item>(), //Default initial value
                fromPreference = {//Function that maps the JSON string in the preferences to our actual list
                    val json = JSONArray(it)
                    ArrayList<Item>(json.length()).apply {
                        for (i in 0 until json.length()) {
                            add(Item.parse(json.getJSONObject(i)))
                        }
                    }
                },
                toPreference = {//Function that maps our list to a JSON string that can be saved in the preferences
                    JSONArray().apply {
                        it.forEach {
                            put(it.toJson())
                        }
                    }.toString()
                }
            )

        //Set activity content
        setContentView(new.frameLayout {

            //Main linear layout
            add.linearLayout(vertical = true) {
                //This is visible only when we have at least an item in our shopping list
                setVisible(list.isNotEmpty())

                //We add the recycler view that will display our items
                add {
                    verticalWeight(1f)
                }.recyclerView(
                    items = list, //The list
                    idProvider = { it.hashCode().toLong() }, //An id for the item. hashCode() is good enough
                    viewCreator = { ItemView(context) }, //Function that creates views
                    viewBinder = { view, item -> //Function that binds an item to a view
                        view.item.value = item //Set the value
                        view.setOnClickListener { //When item is clicked, ask for removal
                            AlertDialog.Builder(context)
                                .setMessage("Remove item \"${item.name}\"?")
                                .setPositiveButton(android.R.string.ok) { _, _ ->
                                    //To remove we use unsafeMutable
                                    //We can use it because we don't care about the difference between unset value and default value
                                    //Read more at https://dataflow.femastudios.com/reference/dataflow-android/v1/shared-preferences/preferencefield
                                    list.unsafeMutable.remove(item)
                                }
                                .setNegativeButton(android.R.string.cancel, null)
                                .show()
                        }
                    }
                )

                //We create a field that calculates the sum of all the item prices
                //We use the fold function
                val sum = list.fold(BigDecimal.ZERO) { acc, item ->
                    acc + (item.price ?: BigDecimal.ZERO)
                }
                //Add view that shows the sum of prices
                add.text("Sum of prices: ".plusF(sum) + " €") {
                    textSize = 22f
                    setPadding(dp(16))
                }
            }

            //Text view that will only be visible when there are no items in the list
            add.text("Shopping list is empty") {
                setVisible(list.isEmpty())
                gravity = Gravity.CENTER
                textSize = 22f
            }

            //Floating Action Button to add an item to the list
            add {
                gravity = Gravity.BOTTOM or Gravity.END
                setMargins(dp(16))
                wrapWidth()
                wrapHeight()
            }.view(FloatingActionButton(context)) {
                setImageResource(R.drawable.ic_add_black_24dp)
                setOnClickListener {
                    //When clicked we ask for the item name and price with an AlertDialog
                    val name = new.editText {
                        hint = "Apples"
                    }
                    val price = new.editText {
                        hint = "2.45"
                        inputType = InputType.TYPE_CLASS_NUMBER or InputType.TYPE_NUMBER_FLAG_DECIMAL
                    }
                    AlertDialog.Builder(context)
                        .setTitle("Add item to shopping list")
                        .setView(new.linearLayout(vertical = true) {
                            setPadding(dp(8))
                            add.text("Name:")
                            add.view(name)
                            add.text("Price:")
                            add.view(price)
                        })
                        .setPositiveButton(android.R.string.ok) { _, _ ->
                            val text = name.text?.toString()
                            val p = price.text?.toString()
                            if (!text.isNullOrBlank()) {
                                //Again here we use unsafeMutable to add this item to the list
                                list.unsafeMutable.add(
                                    Item(
                                        text,
                                        //We also remove unwanted decimal places
                                        if (p.isNullOrBlank()) null else BigDecimal(p).setScale(2, RoundingMode.DOWN)
                                    )
                                )
                            }
                        }
                        .setNegativeButton(android.R.string.cancel, null)
                        .show()
                }
            }
        })
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        //Create menu
        menuInflater.inflate(R.menu.main, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        //Here use again use unsafeMutable to sort or clear the shopping list
        return when (item.itemId) {
            R.id.sortAlphabetically -> {
                list.unsafeMutable.sortBy { it.name }
                true
            }
            R.id.sortByPrice -> {
                list.unsafeMutable.sortBy { it.price }
                true
            }
            R.id.shuffle -> {
                list.unsafeMutable.shuffle()
                true
            }
            R.id.clearList -> {
                AlertDialog.Builder(context)
                    .setMessage("Clear the list?")
                    .setPositiveButton(android.R.string.ok) { _, _ ->
                        list.unsafeMutable.clear()
                    }
                    .setNegativeButton(android.R.string.cancel, null)
                    .show()
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}
