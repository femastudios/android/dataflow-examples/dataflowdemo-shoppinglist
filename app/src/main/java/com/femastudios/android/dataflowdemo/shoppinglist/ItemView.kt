package com.femastudios.android.dataflowdemo.shoppinglist

import android.content.Context
import android.widget.LinearLayout
import androidx.core.view.setPadding
import com.femastudios.android.core.dp
import com.femastudios.android.core.vertical
import com.femastudios.dataflow.FieldWrapper
import com.femastudios.dataflow.android.declarativeui.add
import com.femastudios.dataflow.util.fieldWrapperOfNull

/**
 * View that displays a single item of the shopping list.
 *
 * This view is a [LinearLayout]
 *
 * @param context a context
 */
class ItemView(context: Context) : LinearLayout(context) {

    /**
     * A [FieldWrapper] that represents the item currently shown on this view
     */
    val item: FieldWrapper<Item?> = fieldWrapperOfNull()

    init {
        vertical = true
        setPadding(dp(8))

        //Add ripple effect
        val typedArray = context.theme.obtainStyledAttributes(R.style.AppTheme, intArrayOf(R.attr.selectableItemBackground))
        setBackgroundResource(typedArray.getResourceId(0, 0))


        //Add to this LinearLayout a first text view representing the item name.
        //The item is transformed to get only the name from the item
        add.text(item.transform { it?.name }) {
            textSize = 20f
        }

        //Add to this LinearLayout a second text view representing the item price.
        //The item is transformed to obtain the price text to display
        add.text(item.transform {
            when {
                it == null -> null //If it is null it means there is no item to display, return null
                it.price == null -> "- €" //It it.price is null it means this item has no price, return appropriate text
                else -> it.price.toString() + " €" //Otherwise we have a price, return it
            }
        })
    }
}