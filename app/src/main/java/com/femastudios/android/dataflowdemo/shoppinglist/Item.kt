package com.femastudios.android.dataflowdemo.shoppinglist

import org.json.JSONObject
import java.math.BigDecimal

/**
 * Data class representing a single item of the shopping list
 *
 * @param name name of the item
 * @param price price of the item, can be null
 */
data class Item(val name: String, val price: BigDecimal?) {

    /**
     * Coverts this item to a JSON object, needed for saving
     */
    fun toJson(): JSONObject = JSONObject().apply {
        put("name", name)
        put("price", price?.toPlainString())
    }

    companion object {

        /**
         * Converts the given JSON object to an item, needed for loading
         */
        fun parse(jsonObject: JSONObject) = Item(
            jsonObject.getString("name"),
            if(jsonObject.isNull("price")) null else BigDecimal(jsonObject.getString("price"))
        )
    }
}